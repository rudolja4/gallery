package cz.janrudolf.gallery.ui.search

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.DrawableRes
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import cz.janrudolf.gallery.R
import cz.janrudolf.gallery.databinding.SearchItemViewBinding
import cz.janrudolf.gallery.model.Artwork

class SearchPagedAdapter :
    PagedListAdapter<Artwork, SearchPagedAdapter.SearchViewHolder>(Artwork.diffUtil) {

    var itemActionListener: ItemActionListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SearchViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return SearchViewHolder(SearchItemViewBinding.inflate(inflater))
    }

    override fun onBindViewHolder(holder: SearchViewHolder, position: Int) {
        holder.bind(getItem(position), itemActionListener)
    }

    class SearchViewHolder(private val binding: SearchItemViewBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(artwork: Artwork?, itemActionListener: ItemActionListener?) {
            if (artwork != null) {
                binding.author.text = artwork.authorName
                binding.title.text = artwork.title
                binding.favourite.setImageResource(artwork.favImageResource())
                Picasso
                    .with(binding.root.context)
                    .load(artwork.headerImage!!.url)
                    .resizeDimen(R.dimen.image_thumbnail_width, R.dimen.image_thumbnail_height)
                    .centerCrop()
                    .into(binding.image)

                binding.favourite.setOnClickListener {
                    itemActionListener?.onItemFavoured(artwork)
                }

                binding.root.setOnClickListener {
                    itemActionListener?.onItemSelected(artwork)
                }
            }
        }
    }
}

@DrawableRes
fun Artwork.favImageResource(): Int {
    return if (favourite) R.drawable.ic_star else R.drawable.ic_star_empty
}
