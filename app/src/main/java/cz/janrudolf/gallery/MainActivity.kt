package cz.janrudolf.gallery

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.lifecycle.*
import cz.janrudolf.gallery.ui.detail.DetailFragment
import cz.janrudolf.gallery.ui.search.SearchFragment
import cz.janrudolf.gallery.viewmodel.SearchViewModel

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)

        val viewModel: SearchViewModel by viewModels()
        //{
        //    SearchViewModel.Factory(this, this, intent.extras)
        //}

        viewModel.selected.observe(this, Observer {
            if (it != null) {
                val detailFragment =
                    DetailFragment()
                detailFragment.viewModel = viewModel
                supportFragmentManager.beginTransaction()
                    .add(R.id.container, detailFragment)
                    .addToBackStack("TAG")
                    .commit()
            }
        })

        val searchFragment = SearchFragment()
        searchFragment.viewModel = viewModel

        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                    .replace(R.id.container, searchFragment)
                    .commitNow()
        }
    }
}