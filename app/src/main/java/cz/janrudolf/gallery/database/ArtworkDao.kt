package cz.janrudolf.gallery.database

import androidx.paging.DataSource
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import cz.janrudolf.gallery.model.Artwork

@Dao
interface ArtworkDao {
    @Query("SELECT * FROM artwork")
    fun favouriteArtworks(): DataSource.Factory<Int, Artwork>

    @Query("SELECT id FROM artwork WHERE id in (:ids)")
    suspend fun favouriteIntersect(ids: List<String>): List<String>

    @Insert
    suspend fun insertFavourite(vararg artworks: Artwork)

    @Delete
    suspend fun deleteFavourite(favourite: Artwork)
}