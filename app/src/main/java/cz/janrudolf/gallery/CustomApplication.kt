package cz.janrudolf.gallery

import android.app.Application
import androidx.room.Room
import cz.janrudolf.gallery.database.ArtworkDatabase

class CustomApplication: Application() {
    companion object {
        lateinit var database: ArtworkDatabase
    }

    override fun onCreate() {
        super.onCreate()

        database = Room.databaseBuilder(this, ArtworkDatabase::class.java, "favourite")
            .build()
    }
}