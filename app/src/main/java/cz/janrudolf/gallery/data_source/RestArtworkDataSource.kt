package cz.janrudolf.gallery.data_source

import android.util.Log
import androidx.paging.DataSource
import cz.janrudolf.gallery.CustomApplication
import cz.janrudolf.gallery.api.ArtworkService
import cz.janrudolf.gallery.model.Artwork
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RestArtworkDataSource : ArtworkDataSource() {
    class Factory : DataSource.Factory<Long, Artwork>() {
        override fun create(): DataSource<Long, Artwork> {
            return RestArtworkDataSource()
        }
    }

    companion object {
        const val PAGE_SIZE = 20
    }

    private val service = ArtworkService.service
    private val favouriteDao = CustomApplication.database.artworkDao()

    override fun loadInitial(
        params: LoadInitialParams<Long>,
        callback: LoadInitialCallback<Long, Artwork>
    ) {
        service.listArtworks("", 1, PAGE_SIZE)
            .enqueue(object : Callback<ArtworkService.ListRBody> {
                override fun onResponse(
                    call: Call<ArtworkService.ListRBody>?,
                    response: Response<ArtworkService.ListRBody>?
                ) {
                    val list = response?.body()?.artObjects ?: emptyList()
                    val ids = list.map { artwork -> artwork.id }
                    runBlocking {
                        launch {
                            val favourites = favouriteDao.favouriteIntersect(ids)
                            list.filter { it.id in favourites }.forEach { it.favourite = true }

                            callback.onResult(list, 0, 2)
                        }
                    }
                }

                override fun onFailure(call: Call<ArtworkService.ListRBody>?, t: Throwable?) {
                    Log.e("n32", t.toString())
                }
            })
    }

    override fun loadAfter(params: LoadParams<Long>, callback: LoadCallback<Long, Artwork>) {
        val pageNum = params.key
        service.listArtworks("", pageNum, params.requestedLoadSize)
            .enqueue(object : Callback<ArtworkService.ListRBody> {
                override fun onResponse(
                    call: Call<ArtworkService.ListRBody>?,
                    response: Response<ArtworkService.ListRBody>?
                ) {
                    callback.onResult(response?.body()?.artObjects ?: emptyList(), pageNum + 1)
                }

                override fun onFailure(call: Call<ArtworkService.ListRBody>?, t: Throwable?) {
                    Log.e("n32", t.toString())
                }
            })
    }

    override fun loadBefore(params: LoadParams<Long>, callback: LoadCallback<Long, Artwork>) {}
}