package cz.janrudolf.gallery.viewmodel

import androidx.lifecycle.*
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import androidx.paging.toLiveData
import cz.janrudolf.gallery.CustomApplication
import cz.janrudolf.gallery.api.ArtworkService
import cz.janrudolf.gallery.data_source.RestArtworkDataSource
import cz.janrudolf.gallery.database.ArtworkDao
import cz.janrudolf.gallery.model.Artwork
import cz.janrudolf.gallery.ui.search.ItemActionListener
import cz.janrudolf.gallery.ui.search.SearchContext
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SearchViewModel() : ViewModel(),
    ItemActionListener {
    private val artworkDao: ArtworkDao = CustomApplication.database.artworkDao()

    val section = MutableLiveData<SearchContext>(
        SearchContext.SEARCH
    )

    //searchList
    val list = LivePagedListBuilder(RestArtworkDataSource.Factory(), RestArtworkDataSource.PAGE_SIZE)
        .build()

    val favouriteList: LiveData<PagedList<Artwork>> =
        artworkDao.favouriteArtworks().toLiveData(pageSize = 20)

    val currentList: LiveData<PagedList<Artwork>> get() =
        if (section.value == SearchContext.SEARCH) list else favouriteList

    val selected = MutableLiveData<Artwork>()

    val selectedDetail = MutableLiveData<Artwork>()

    val itemChanged = MutableLiveData<Artwork>()

    private val artworkService = ArtworkService.service

    init {
        selected.observeForever {
            if (it == null) {
                selectedDetail.value = null
            } else {
                artworkService.artworkDetail(it.objectNumber)
                    .enqueue(object : Callback<ArtworkService.DetailRBody?> {
                        override fun onResponse(
                            call: Call<ArtworkService.DetailRBody?>?,
                            response: Response<ArtworkService.DetailRBody?>?
                        ) {
                            if (response?.code() == 200) {
                                selectedDetail.postValue(response.body()?.artwork)
                            }
                        }

                        override fun onFailure(call: Call<ArtworkService.DetailRBody?>?, t: Throwable?) {
                            TODO("Not yet implemented")
                        }
                })
            }
        }
    }

    override fun onItemSelected(item: Artwork) {
        if (selected.value == null)
            selected.value = item
    }

    override fun onItemFavoured(item: Artwork) {
        viewModelScope.launch {
            item.favourite = !item.favourite
            arrayOf(list, favouriteList)
                .map { l -> Pair(l, l.value?.indexOf(item) ?: -1) }
                .forEach { if (it.second != -1) it.first.value!![it.second]!!.favourite = item.favourite }
            if (item.favourite)
                artworkDao.insertFavourite(item)
            else
                artworkDao.deleteFavourite(item)
            itemChanged.postValue(item)
        }
    }

    /*class Factory(
        owner: SavedStateRegistryOwner,
        private val context: Context,
        defaultArgs: Bundle? = null
    ) : AbstractSavedStateViewModelFactory(owner, defaultArgs) {
        override fun <T : ViewModel?> create(
            key: String,
            modelClass: Class<T>,
            handle: SavedStateHandle
        ): T {
            val database = Room.databaseBuilder(context, ArtworkDatabase::class.java, "favourite2")
                .build()
            return SearchViewModel(database.artworkDao()) as T
        }
    }*/
}