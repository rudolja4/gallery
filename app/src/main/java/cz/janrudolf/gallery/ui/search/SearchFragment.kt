package cz.janrudolf.gallery.ui.search

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import cz.janrudolf.gallery.R
import cz.janrudolf.gallery.databinding.SearchFragmentBinding
import cz.janrudolf.gallery.viewmodel.SearchViewModel

class SearchFragment : Fragment() {
    private var _binding: SearchFragmentBinding? = null
    private val binding get() = _binding!!

    lateinit var viewModel: SearchViewModel

    private lateinit var searchAdapter: SearchPagedAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = SearchFragmentBinding.inflate(inflater, container, false)

        searchAdapter = SearchPagedAdapter()
        searchAdapter.itemActionListener = viewModel

        binding.list.apply {
            layoutManager = LinearLayoutManager(context)
        }

        binding.navigation.setOnNavigationItemSelectedListener {
            val state: SearchContext = when (it.itemId) {
                R.id.favourite -> SearchContext.FAVOURITES
                R.id.search -> SearchContext.SEARCH
                else -> return@setOnNavigationItemSelectedListener false
            }

            viewModel.section.value = state

            return@setOnNavigationItemSelectedListener true
        }

        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel.section.observe(viewLifecycleOwner, Observer { setSearchContext(it) })

        viewModel.list.observe(viewLifecycleOwner, Observer {
            if (viewModel.section.value == SearchContext.SEARCH)
                searchAdapter.submitList(it)
        })

        viewModel.favouriteList.observe(viewLifecycleOwner, Observer {
            if (viewModel.section.value == SearchContext.FAVOURITES)
                searchAdapter.submitList(it)
        })

        viewModel.itemChanged.observe(viewLifecycleOwner, Observer {
            val index: Int = viewModel.currentList.value!!.indexOf(it)
            if (index != -1)
                searchAdapter.notifyItemChanged(index)
        })

        binding.list.adapter = searchAdapter
    }

    private fun setSearchContext(section: SearchContext) {
        val list = when (section) {
            SearchContext.FAVOURITES -> viewModel.favouriteList
            SearchContext.SEARCH -> viewModel.list
        }

        //Submitting without clear causes exception due to different datasource key types
        searchAdapter.submitList(null)
        searchAdapter.notifyDataSetChanged()
        searchAdapter.submitList(list.value)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}