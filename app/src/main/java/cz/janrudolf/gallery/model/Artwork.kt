package cz.janrudolf.gallery.model

import androidx.recyclerview.widget.DiffUtil
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
data class Artwork(
    @PrimaryKey
    val id: String,
    val objectNumber: String,
    val title: String,
    @Embedded(prefix="header")
    val headerImage: ArtworkImage?,
    @Embedded(prefix="web")
    val webImage: ArtworkImage,
    @SerializedName("principalOrFirstMaker")
    val authorName: String,
    @Embedded
    val dating: Dating?,
    @SerializedName("plaqueDescriptionEnglish")
    val description: String?,
    var favourite: Boolean = false
) {

    @Entity
    data class ArtworkImage(
        @PrimaryKey val url: String,
        val width: Int,
        val height: Int
    )

    @Entity
    data class Dating(@SerializedName("sortingDate") val year: Int)

    companion object {
        @JvmStatic
        val diffUtil = object : DiffUtil.ItemCallback<Artwork>() {
            override fun areItemsTheSame(oldItem: Artwork, newItem: Artwork) =
                oldItem.id == newItem.id

            override fun areContentsTheSame(oldItem: Artwork, newItem: Artwork) =
                areItemsTheSame(oldItem, newItem)
        }
    }
}