package cz.janrudolf.gallery.api

import com.google.gson.annotations.SerializedName
import cz.janrudolf.gallery.model.Artwork
import okhttp3.HttpUrl
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ArtworkService {
    companion object {
        private const val BASE_URL = "https://www.rijksmuseum.nl/api/en/"
        private const val API_KEY = "Wki1W5at"

        private val apiKeyInterceptor: Interceptor = Interceptor { chain ->
            val originalRequest: Request = chain.request()
            val originalHttpUrl: HttpUrl = originalRequest.url()
            val url = originalHttpUrl.newBuilder()
                //adds api key to every request query
                .addQueryParameter("key", API_KEY)
                //assures only image data
                .addQueryParameter("imgonly", "True")
                .build()
            val request: Request = originalRequest.newBuilder().url(url).build()
            chain.proceed(request)
        }

        private val client: OkHttpClient = OkHttpClient.Builder()
            .addInterceptor(apiKeyInterceptor)
            .build()

        private val retrofit: Retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(client)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        val service get(): ArtworkService = retrofit.create(ArtworkService::class.java)
    }

    data class ListRBody(val count: Long, val artObjects: List<Artwork>)

    @GET("collection")
    fun listArtworks(
        @Query("q") query: String,
        @Query("p") pageNumber: Long = 0,
        @Query("ps") pageSize: Int = 10
    ): Call<ListRBody>

    data class DetailRBody(@SerializedName("artObject") val artwork: Artwork)

    @GET("collection/{object-number}")
    fun artworkDetail(
        @Path("object-number") objectNumber: String
    ): Call<DetailRBody>
}