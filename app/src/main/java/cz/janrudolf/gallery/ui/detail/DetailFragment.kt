package cz.janrudolf.gallery.ui.detail

import android.os.Bundle
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.squareup.picasso.Picasso
import cz.janrudolf.gallery.databinding.DetailFragmentBinding
import cz.janrudolf.gallery.model.Artwork
import cz.janrudolf.gallery.viewmodel.SearchViewModel

class DetailFragment : Fragment() {

    private var _binding: DetailFragmentBinding? = null
    private val binding get() = _binding!!

    lateinit var viewModel: SearchViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = DetailFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel.selected.observe(viewLifecycleOwner, Observer {
            //use only if detail is not loaded yet
            if (viewModel.selectedDetail.value == null && it != null)
                bind(it)
        })

        viewModel.selectedDetail.observe(viewLifecycleOwner, Observer {
            if (it != null)
                bindRest(it)
        })
    }

    private fun bind(detailedArtwork: Artwork) {
        //prevents the blink when the image is downloaded
        val displayMetrics = DisplayMetrics()
        activity!!.windowManager.defaultDisplay.getMetrics(displayMetrics)

        val image = detailedArtwork.webImage
        val ratio = image.height.toFloat() / image.width.toFloat()
        val viewWidth = displayMetrics.widthPixels
        val viewHeight = (viewWidth.toFloat() * ratio).toInt()

        val params = LinearLayout.LayoutParams(viewWidth, viewHeight)
        binding.image.layoutParams = params
        binding.image.requestLayout()

        Picasso.with(context).load(detailedArtwork.webImage.url).into(binding.image)
        binding.author.text = detailedArtwork.authorName
        binding.title.text = detailedArtwork.title
        binding.description.text = detailedArtwork.description
        binding.year.text =
            if (detailedArtwork.dating == null) "" else detailedArtwork.dating.year.toString()
    }

    /**
     * Binds properties that are not initializable by basic artwork
     */
    private fun bindRest(detailedArtwork: Artwork) {
        binding.description.text = detailedArtwork.description
        binding.year.text = detailedArtwork.dating?.year.toString()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        viewModel.selected.postValue(null)
        _binding = null
    }
}