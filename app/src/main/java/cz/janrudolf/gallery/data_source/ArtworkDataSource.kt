package cz.janrudolf.gallery.data_source

import androidx.paging.PageKeyedDataSource
import cz.janrudolf.gallery.model.Artwork

abstract class ArtworkDataSource: PageKeyedDataSource<Long, Artwork>()