package cz.janrudolf.gallery.ui.search

enum class SearchContext {
    SEARCH, FAVOURITES
}