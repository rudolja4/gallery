package cz.janrudolf.gallery.ui.search

import cz.janrudolf.gallery.model.Artwork

interface ItemActionListener {
    fun onItemSelected(item: Artwork)

    fun onItemFavoured(item: Artwork)
}