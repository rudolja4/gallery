package cz.janrudolf.gallery.database

import androidx.room.Database
import androidx.room.RoomDatabase
import cz.janrudolf.gallery.model.Artwork

@Database(entities = [Artwork::class], version = 1)
abstract class ArtworkDatabase: RoomDatabase() {
    abstract fun artworkDao(): ArtworkDao
}